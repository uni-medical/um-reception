package com.uni.medical.umreception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmReceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(UmReceptionApplication.class, args);
    }
}
